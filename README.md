<h1 align="center">Product Management API</h1>
<br>

# ✨ Technologies

This project was developed with the following technologies: 

- [Node.js]
- [Typescript]
- [Express]
- [Postgres]
- [Typeorm]
- [TSyring]

# 💻 Project

Project Management CRUD

# 🚀 How to execute

- Clone the repository 
- Install or create a image Postgres Database

# To run local

- Run `yarn` to install dependencies
- Change credentials and configs database in `ormconfig.json` file to run Postgres 
- Create a database  with name `product_management`
- Execute `yarn typeorm migration:run` to create tables
- Run `yarn dev` to initiate local api

# Routes

- BaseURL: `localhost:3000/`

# Products
- POST ("/products")
- GET ("/products/product_id)
- PUT ("/products/product_id)
- DELETE "/products/product_id)

# Categories
- POST ("/categories")
- GET ("/categories/category_id)
- PUT ("/categories/category_id)
- DELETE "/categories/category_id)

# Schemas

# Products
```json
{
    "name": "product_name",
    "SKU": "2hfi74ghjd932",
    "description": "product_description",
    "category_id": "category_id",
    "price": 9999.99,
    "quantity": 999
}
```

# Categories
```json
{
	"name": "category_name",
	"description": "category_description"
}
```

# :closed_book: License <a name="-license" style="text-decoration:none"></a>


Released in 2021. This project is under the MIT license LICENSE

Made with love by Arthur Machado