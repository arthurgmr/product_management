import { container } from "tsyringe";
import { CategoryRepository } from "../../modules/products/infra/repositories/CategoryRepository";

import { ProductRepository } from "../../modules/products/infra/repositories/ProductRepository";
import { ICategoryRepository } from "../../modules/products/repositories/ICategoryRepository";
import { IProductRepository } from "../../modules/products/repositories/IProductRepository";

container.registerSingleton<IProductRepository>(
    "ProductRepository",
    ProductRepository
);

container.registerSingleton<ICategoryRepository>(
    "CategoryRepository",
    CategoryRepository
);
