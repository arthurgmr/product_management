import { Router } from "express";
import { CreateCategoryController } from "../../../../modules/products/useCases/createCategory/CreateCategoryController";
import { DeleteCategoryController } from "../../../../modules/products/useCases/deleteCategory/DeleteCategoryController";
import { ReadCategoryController } from "../../../../modules/products/useCases/readCategory/ReadCategoryController";
import { UpdateCategoryController } from "../../../../modules/products/useCases/updateCategory/UpdateCategoryController";

const categoryRoutes = Router();

const createCategoryController = new CreateCategoryController();
const readCategoryController = new ReadCategoryController();
const updateCategoryController = new UpdateCategoryController();
const deleteCategoryController = new DeleteCategoryController();

categoryRoutes.post("/", createCategoryController.handle);
categoryRoutes.get("/:category_id", readCategoryController.handle);
categoryRoutes.put("/:category_id", updateCategoryController.handle);
categoryRoutes.delete("/:category_id", deleteCategoryController.handle);

export { categoryRoutes };