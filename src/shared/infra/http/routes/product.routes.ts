import { Router } from "express";
import { CreateProductController } from "../../../../modules/products/useCases/createProduct/CreateProductController";
import { DeleteProductController } from "../../../../modules/products/useCases/deleteProduct/DeleteProductController";
import { ReadProductController } from "../../../../modules/products/useCases/readProduct/ReadProductController";
import { UpdateProductController } from "../../../../modules/products/useCases/updateProduct/UpdateProductController";

const productRoutes = Router();

const createProductController = new CreateProductController();
const readProductController = new ReadProductController();
const updateProductController = new UpdateProductController();
const deleteProductController = new DeleteProductController();

productRoutes.post("/", createProductController.handle);
productRoutes.get("/:product_id", readProductController.handle);
productRoutes.put("/:product_id", updateProductController.handle);
productRoutes.delete("/:product_id", deleteProductController.handle);



export { productRoutes };