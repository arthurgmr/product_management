import { ICategoryDTO } from "../dtos/ICategoryDTO";
import { Category } from "../infra/typeorm/entities/Category";


interface ICategoryRepository {
    create(data: ICategoryDTO): Promise<Category>;
    read(id: string): Promise<Category>;
    update(data: ICategoryDTO, category_id: string): Promise<Category>;
    delete(id: string): Promise<void>;
    findById(id: string): Promise<Category>;
    findByName(name: string): Promise<Category>;
    findAll(): Promise<Category[]>;
}

export { ICategoryRepository };