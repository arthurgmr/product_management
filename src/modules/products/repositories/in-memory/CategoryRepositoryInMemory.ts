import { ICategoryDTO } from "../../dtos/ICategoryDTO";
import { Category } from "../../infra/typeorm/entities/Category";
import { ICategoryRepository } from "../ICategoryRepository";


class CategoryRepositoryInMemory implements ICategoryRepository {

    category: Category[] = [];

    async create({ name, description }: ICategoryDTO): Promise<Category> {
        const category = new Category();

        Object.assign(category, { name, description });

        this.category.push(category);

        return category;
    };

    async read(id: string): Promise<Category> {
        return this.category.find((category) => category.id === id);
    };

    async update({ name, description }: ICategoryDTO, category: Category ): Promise<Category> {
        
        Object.assign(category, { name, description });

        category.updated_at = new Date();

        return category;
    };
    
    async delete(id: string): Promise<void> {
        this.category.filter((category) => category.id !== id);
    };

    async findAll(): Promise<Category[]> {
        return this.category;
    };

    async findById(id: string): Promise<Category> {
        return this.category.find((category) => category.id === id);
    }

    async findByName(name: string): Promise<Category> {
        return this.category.find((category) => category.name === name);
    }

};

export { CategoryRepositoryInMemory };