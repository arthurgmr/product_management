import { IProductDTO } from "../../dtos/IProductDTO";
import { Product } from "../../infra/typeorm/entities/Product";
import { IProductRepository } from "../IProductRepository";


class ProductRepositoryInMemory implements IProductRepository {

    product: Product[] = [];

    async create({ 
        name, 
        SKU, 
        category_id, 
        description, 
        price, 
        quantity }: IProductDTO): Promise<Product> {
        const product = new Product();

        Object.assign(product, {
            name, 
            SKU, 
            category_id, 
            description, 
            price, 
            quantity
        });

        this.product.push(product);

        return product;
    };

    async read(id: string): Promise<Product> {
        return this.product.find((product) => product.id === id);
    };

    async update({
        id,
        name,
        SKU,
        category_id,
        description,
        price,
        quantity}: IProductDTO): Promise<Product> {
        
        const product = this.product.find((product) => product.id === id);

        Object.assign(product, {
            name,
            SKU,
            category_id,
            description,
            price,
            quantity
        })

        product.updated_at = new Date();

        return product;
    };
    
    async delete(id: string): Promise<void> {
        this.product.filter((product) => product.id !== id);
    };

    async findBySKU(SKU: string): Promise<Product> {
        return this.product.find((product) => product.SKU === SKU);
    };

};

export { ProductRepositoryInMemory };