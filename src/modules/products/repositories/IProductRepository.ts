import { IProductDTO } from "../dtos/IProductDTO";
import { Product } from "../infra/typeorm/entities/Product";


interface IProductRepository {
    create(data: IProductDTO): Promise<Product>;
    read(id: string): Promise<Product>;
    update(data: IProductDTO, product_id: string): Promise<Product>;
    delete(id: string): Promise<void>;
    findBySKU(SKU: string): Promise<Product>;
}

export { IProductRepository };