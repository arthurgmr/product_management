import { inject, injectable } from "tsyringe";
import { AppError } from "../../../../shared/errors/AppError";
import { IProductDTO } from "../../dtos/IProductDTO";
import { Product } from "../../infra/typeorm/entities/Product";
import { ICategoryRepository } from "../../repositories/ICategoryRepository";
import { IProductRepository } from "../../repositories/IProductRepository";

@injectable()
class CreateProductUseCase {

    constructor(
        @inject("ProductRepository")
        private productRepository: IProductRepository,
        @inject("CategoryRepository")
        private categoryRepository: ICategoryRepository ) {};

    async execute( data:IProductDTO): Promise<Product> {
            // check if category exists;
            const categoryExists = await this.categoryRepository.findById(data.category_id);
            if(!categoryExists) {
                throw new AppError("Category is invalid!")
            };
            // check if product already exists(SKU);
            const productExists = await this.productRepository.findBySKU(data.SKU);
            if(productExists) {
                throw new AppError("Product already exists!")
            };
            // create a product;
            const product = await this.productRepository.create(data);
            return product;
    };
};

export { CreateProductUseCase };