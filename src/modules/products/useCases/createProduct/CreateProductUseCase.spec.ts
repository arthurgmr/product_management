import { AppError } from "../../../../shared/errors/AppError";
import { CategoryRepositoryInMemory } from "../../repositories/in-memory/CategoryRepositoryInMemory";
import { ProductRepositoryInMemory } from "../../repositories/in-memory/ProductRepositoryInMemory";
import { CreateProductUseCase } from "./CreateProductUseCase";


let createProductUseCase: CreateProductUseCase;
let productRepositoryInMemory: ProductRepositoryInMemory;
let categoryRepositoryInMemory: CategoryRepositoryInMemory;

describe("Create a Product", () => {
    beforeEach (() => {
        productRepositoryInMemory = new ProductRepositoryInMemory();
        categoryRepositoryInMemory = new CategoryRepositoryInMemory();
        createProductUseCase = new CreateProductUseCase(
            productRepositoryInMemory, categoryRepositoryInMemory
        );
    });

    it("Should be able to create a new product", async () => {
        const dataCategory = {
            name: "Technology",
            description: "Technology related items."
        };
        const category = await categoryRepositoryInMemory.create(dataCategory);

        const productData = {
            name: "Notebook GameProc",
            SKU: "u272904nm23fnd2vl4ld2018",
            category_id: category.id,
            description: "Notebook Gamer Intel Core i9-2816U",
            price: 1599.99,
            quantity: 5
        };
        const product = await createProductUseCase.execute(productData);

        expect(product).toHaveProperty("id");
    });

    it("Should not be able to create a new product with a invalid category", async () => {
        const productData = {
            name: "Notebook GameProc",
            SKU: "u272904nm23fnd2vl4ld2018",
            category_id: "invalid_category",
            description: "Notebook Gamer Intel Core i9-2816U",
            price: 1599.99,
            quantity: 5
        }; 
        await expect(
            createProductUseCase.execute(productData)
            ).rejects.toEqual(new AppError("Category is invalid!"));
    });

    it("Should not be able to create a new product with same SKU code", async () => {
        const dataCategory = {
            name: "Technology",
            description: "Technology related items."
        };
        const category = await categoryRepositoryInMemory.create(dataCategory);

        const productData = {
            name: "Notebook GameProc",
            SKU: "u272904nm23fnd2vl4ld2018",
            category_id: category.id,
            description: "Notebook Gamer Intel Core i9-2816U",
            price: 1599.99,
            quantity: 5
        };         
        await createProductUseCase.execute(productData);

        const sameProductData = {
            name: "Notebook GameProc",
            SKU: "u272904nm23fnd2vl4ld2018",
            category_id: category.id,
            description: "Notebook Gamer Intel Core i9-2816U",
            price: 1599.99,
            quantity: 5
        };
        
        await expect(
            createProductUseCase.execute(sameProductData)
            ).rejects.toEqual(new AppError("Product already exists!"));
    });
});