import { inject, injectable } from "tsyringe";
import { AppError } from "../../../../shared/errors/AppError";
import { ICategoryRepository } from "../../repositories/ICategoryRepository";

@injectable()
class DeleteCategoryUseCase {

    constructor (
        @inject("CategoryRepository")
        private categoryRepository: ICategoryRepository) 
    {};
    
    async execute(id:string): Promise<void> {
        const category = await this.categoryRepository.read(id);
        //validation
        if(!category) {
            throw new AppError("Category id invalid!")
        }
        
        await this.categoryRepository.delete(id);
    };
};

export { DeleteCategoryUseCase };
