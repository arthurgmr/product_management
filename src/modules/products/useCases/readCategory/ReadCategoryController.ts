import { Request, Response } from "express";
import { container } from "tsyringe";
import { ReadCategoryUseCase } from "./ReadCategoryUseCase";

class ReadCategoryController {
    async handle(request: Request, response: Response): Promise<Response> {
        const { category_id } = request.params;

        const readCategoryUseCase = container.resolve(ReadCategoryUseCase);

        const category = await readCategoryUseCase.execute(category_id);
        
        return response.send(category);
    }; 
};

export { ReadCategoryController };