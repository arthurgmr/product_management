import { inject, injectable } from "tsyringe";
import { AppError } from "../../../../shared/errors/AppError";
import { Category } from "../../infra/typeorm/entities/Category";
import { ICategoryRepository } from "../../repositories/ICategoryRepository";

@injectable()
class ReadCategoryUseCase {

    constructor (
        @inject("CategoryRepository")
        private categoryRepository: ICategoryRepository) 
    {};
    
    async execute(category_id:string): Promise<Category> {
        const category = await this.categoryRepository.read(category_id);
        //validation
        if(!category) {
            throw new AppError("Category id invalid!")
        };

        return category;
    };
};

export { ReadCategoryUseCase };
