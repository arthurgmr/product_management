import { inject, injectable } from "tsyringe";
import { AppError } from "../../../../shared/errors/AppError";
import { Product } from "../../infra/typeorm/entities/Product";
import { IProductRepository } from "../../repositories/IProductRepository";

@injectable()
class ReadProductUseCase {

    constructor (
        @inject("ProductRepository")
        private productRepository: IProductRepository) 
    {};
    
    async execute(product_id:string): Promise<Product> {
        const product = await this.productRepository.read(product_id);
        //validation
        if(!product) {
            throw new AppError("Product id invalid!")
        };

        return product;
    };
};

export { ReadProductUseCase };
