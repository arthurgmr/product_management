import { Request, Response } from "express";
import { container } from "tsyringe";
import { ReadProductUseCase } from "./ReadProductUseCase";

class ReadProductController {
    async handle(request: Request, response: Response): Promise<Response> {
        const { product_id } = request.params;

        const readProductUseCase = container.resolve(ReadProductUseCase);

        const product = await readProductUseCase.execute(product_id);
        
        return response.send(product);
    }; 
};

export { ReadProductController };