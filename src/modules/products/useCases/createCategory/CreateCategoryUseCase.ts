import { inject, injectable } from "tsyringe";
import { AppError } from "../../../../shared/errors/AppError";
import { ICategoryDTO } from "../../dtos/ICategoryDTO";
import { Category } from "../../infra/typeorm/entities/Category";
import { ICategoryRepository } from "../../repositories/ICategoryRepository";

@injectable()
class CreateCategoryUseCase {

    constructor(
        @inject("CategoryRepository")
        private categoryRepository: ICategoryRepository) 
    {};

    async execute(data:ICategoryDTO): Promise<Category> {
        // check if category already exists;
        const categoryExists = await this.categoryRepository.findByName(data.name);
        if(categoryExists) {
            throw new AppError("Category already exists!")
        }
        // create a category;
        const category = await this.categoryRepository.create(data);
        return category;
    }
};

export { CreateCategoryUseCase };