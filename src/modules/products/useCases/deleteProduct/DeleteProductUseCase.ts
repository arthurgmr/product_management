import { inject, injectable } from "tsyringe";
import { AppError } from "../../../../shared/errors/AppError";
import { IProductRepository } from "../../repositories/IProductRepository";

@injectable()
class DeleteProductUseCase {

    constructor (
        @inject("ProductRepository")
        private productRepository: IProductRepository) 
    {};
    
    async execute(id:string): Promise<void> {
        const product = await this.productRepository.read(id);
        //validation
        if(!product) {
            throw new AppError("Product id invalid!")
        }
        
        await this.productRepository.delete(id);
    };
};

export { DeleteProductUseCase };
