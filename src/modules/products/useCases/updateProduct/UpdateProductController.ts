import { Request, Response } from "express";
import { container } from "tsyringe";
import { UpdateProductUseCase } from "./UpdateProductUseCase";

class UpdateProductController { 
    async handle(request: Request, response: Response): Promise<Response> {
        const { name, SKU, description, category_id, price, quantity } = request.body;
        const { product_id } = request.params

        const updateProductUseCase = container.resolve(UpdateProductUseCase);

        const productUpdated = await updateProductUseCase.execute({  name, SKU, description, category_id, price, quantity }, product_id)

        return response.send(productUpdated);

    }
};

export { UpdateProductController };