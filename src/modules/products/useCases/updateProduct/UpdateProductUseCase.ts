import { inject, injectable } from "tsyringe";
import { AppError } from "../../../../shared/errors/AppError";
import { IProductDTO } from "../../dtos/IProductDTO";
import { Product } from "../../infra/typeorm/entities/Product";
import { ICategoryRepository } from "../../repositories/ICategoryRepository";
import { IProductRepository } from "../../repositories/IProductRepository";

@injectable()
class UpdateProductUseCase {

    constructor (
        @inject("ProductRepository")
        private productRepository: IProductRepository
    ) {};

    async execute(data:IProductDTO, product_id: string): Promise<Product> {
        // product validation;
        const product = await this.productRepository.read(product_id);
        if(!product) {
            throw new AppError("Product is invalid")
        };

        const productUpdated = await this.productRepository.update(data, product_id);

        return productUpdated;
    };
};

export { UpdateProductUseCase };