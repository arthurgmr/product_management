import { inject, injectable } from "tsyringe";
import { ICategoryDTO } from "../../dtos/ICategoryDTO";
import { Category } from "../../infra/typeorm/entities/Category";
import { ICategoryRepository } from "../../repositories/ICategoryRepository";

@injectable()
class UpdateCategoryUseCase {

    constructor (
        @inject("CategoryRepository")
        private categoryRepository: ICategoryRepository
    ) {};

    async execute(data:ICategoryDTO, category_id: string): Promise<Category> {

        const categoryUpdated = await this.categoryRepository.update(data, category_id);

        return categoryUpdated;
    };
};

export { UpdateCategoryUseCase };