import { Request, Response } from "express";
import { container } from "tsyringe";
import { UpdateCategoryUseCase } from "./UpdateCategoryUseCase";

class UpdateCategoryController { 
    async handle(request: Request, response: Response): Promise<Response> {
        const { name, description } = request.body;
        const { category_id } = request.params

        const updateCategoryUseCase = container.resolve(UpdateCategoryUseCase);

        const categoryUpdated = await updateCategoryUseCase.execute({ name, description }, category_id)

        return response.send(categoryUpdated);

    }
};

export { UpdateCategoryController };