
interface ICategoryDTO {
    id?: string;
    name: string;
    description: string;
    created_at?: Date;
    updated_at?: Date;
};

export { ICategoryDTO };