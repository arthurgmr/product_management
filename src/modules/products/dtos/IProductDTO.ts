
interface IProductDTO {
    id?: string;
    name: string;
    SKU: string;
    price: Number;
    description: string;
    quantity: Number;
    category_id: string;
    created_at?: Date;
    updated_at?: Date;
};

export { IProductDTO };