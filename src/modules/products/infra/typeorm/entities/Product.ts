import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryColumn, UpdateDateColumn } from "typeorm";
import { v4 as uuidV4 } from "uuid";
import { Category } from "./Category";

@Entity("products")
class Product {
    @PrimaryColumn()
    id: string;
    
    @Column()
    name: string;

    @Column()
    SKU: string;
    
    @Column()
    description: string;

    @Column()
    price: Number;

    @Column()
    quantity: Number;

    @ManyToOne(() => Category)
    @JoinColumn({ name: "category_id" })
    category: Category;

    @Column()
    category_id: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    constructor() {
        if (!this.id) {
            this.id = uuidV4();
        };
    };
};

export { Product };