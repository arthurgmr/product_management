import { getRepository, Repository } from "typeorm";
import { IProductDTO } from "../../dtos/IProductDTO";
import { IProductRepository } from "../../repositories/IProductRepository";
import { Product } from "../typeorm/entities/Product";


class ProductRepository implements IProductRepository {
    private repository: Repository<Product>;

    constructor() {
        this.repository = getRepository(Product);
    }

    async create(data: IProductDTO): Promise<Product> {
        const product = this.repository.create(data);

        await this.repository.save(product);

        return product;
    }
    async read(product_id: string): Promise<any> {
        const product = await this.repository
            .createQueryBuilder("products")
            .leftJoinAndSelect("products.category", "categories")
            .select([
                "products.*",
                "categories.name AS category_name",
                "categories.description AS category_description"
            ])
            .where("products.id = :product_id", { product_id })
            .getRawOne()

        return product;
    }
    async update({
        name,
        SKU,
        description,
        category_id,
        price,
        quantity,
    }: IProductDTO, product_id: string): Promise<Product> {
        await this.repository.update(product_id, {
            name,
            SKU,
            description,
            category_id,
            price,
            quantity,
         });

         return await this.repository.findOne(product_id);
    }
    async delete(id: string): Promise<void> {
        await this.repository.delete(id);
    }
    async findBySKU(SKU: string): Promise<Product> {
        return await this.repository.findOne({where: { SKU }});
    }
};

export { ProductRepository };