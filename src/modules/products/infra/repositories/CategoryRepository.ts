import { getRepository, Repository } from "typeorm";
import { ICategoryDTO } from "../../dtos/ICategoryDTO";
import { ICategoryRepository } from "../../repositories/ICategoryRepository";
import { Category } from "../typeorm/entities/Category";


class CategoryRepository implements ICategoryRepository {
    private repository: Repository<Category>;

    constructor() {
        this.repository = getRepository(Category);
    }

    async create(data: ICategoryDTO): Promise<Category> {
        const category = this.repository.create(data);

        await this.repository.save(category);

        return category;
    }
    async read(id: string): Promise<Category> {
        const category = await this.repository.findOne(id);
        return category;
    }
    async update({
        name,
        description,
    }: ICategoryDTO, category_id: string): Promise<Category> {
        await this.repository.update(category_id, {
            name,
            description
         });

         return await this.repository.findOne(category_id);
    }
    async delete(id: string): Promise<void> {
        await this.repository.delete(id);
    }
    async findAll(): Promise<Category[]> {
        return await this.repository.find();
    }
    async findById(id: string): Promise<Category> {
        return await this.repository.findOne(id);
    }
    async findByName(name: string): Promise<Category> {
        return await this.repository.findOne({ where: { name }});
    }

};

export { CategoryRepository };